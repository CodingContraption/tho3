<!DOCTYPE>
<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="java.util.ArrayList" %>
	<%@ page import="voorraadbeheer.Artikel" %>
	<%@ page import="voorraadbeheer.Onderdeel" %>
	<%@ page import="voorraadbeheer.Brandstof" %>
	<div id="Menu">
		<ul>
			<li><a href='index.jsp'>Home</a></li>
			<li><a href='voegArtikelToe.jsp'>Artikel toevoegen</a></li>
		</ul>
	</div>
	<div id="Content">
		<table>
		<tr>
		<th>Naam</th><th>Type</th><th>Aantal/Liter</th><th>Minimum aantal/liter</th><th>Prijs per stuk/liter</th>
		</tr>
		<% 
		ArrayList<Artikel> contents = (ArrayList<Artikel>)request.getAttribute("contents"); 
		if(contents.size() > 0){
			for(Artikel a: contents) { %>
				<tr>
					<% if(a.getArtikelType().equals("Brandstof")) {
						Brandstof b = (Brandstof) a;%>
						<td><a href="Artikel?name=<%= a.getName() %>"><%= a.getName() %></a></td><td><%= a.getArtikelType() %></td><td><%= b.getLiter() %></td><td><%= b.getMinLiter() %></td><td><%= b.getPrijsPerLiter() %></td> 
					<% } else { 
						Onderdeel o = (Onderdeel) a;%>
						<td><a href="Artikel?name=<%= a.getName() %>"><%= a.getName() %></a></td><td><%= a.getArtikelType() %></td><td><%= o.getAantal() %></td><td><%= o.getMinAantal() %></td><td><%= o.getPrijsPerStuk() %></td> 
					<% } %>
				</tr>
			<% } 
		}else{%>
			<tr>
				<td>Geen Artikelen gevonden</td>
			</tr>
		<% } %>
		</table>
		<form method="POST" action="Search">
			<input type="text" name="name">
			<input type="submit" value="Zoek">		
		</form>
	</div>
</body>
<html>