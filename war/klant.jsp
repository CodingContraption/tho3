<!DOCTYPE>
<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="klantenbinding.Klant" %>
	<%@ page import="klantenbinding.Auto" %>
	<% Klant a = (Klant)request.getAttribute("klant"); %>
	<div id="Menu">
		<ul>
			<li><a href='index.jsp'>Home</a></li>
			<li><a href="voegAutoToe.jsp?name=<%= a.getNaam() %>"> voeg een auto toe</a></li> 
			<li><a href='klanten.jsp'>Terug naar klanten overzicht</a></li>
		</ul>
	</div>
	<div id="Content">
	
	<form method="POST" action="EditKlant?name=<%= a.getNaam() %>">
			<fieldset>
				<input type="text" name="naam" value="<%= a.getNaam() %>" placeholder="Vul hier de naam van de Klant in" /><br>
				<input type="text" name="adres" value="<%= a.getAdres() %>" placeholder="Vul hier het adres in" /><br>
				<input type="email" name="email" value="<%= a.getEmail() %>" placeholder="Vul hier het email in" /><br>
				<select name="vasteklant">
				<% if(a.getVasteKlant()){ %>
					<option value="true" selected>Waar</option>
					<option value="false">Niet waar</option>
				<% }else{ %>
					<option value="true">Waar</option>
					<option value="false" selected>Niet waar</option>
				<% } %>
				</select>
			</fieldset>
			<input type="submit" value="Edit" />
		</form>	
		<form method="POST" action="VerstuurBrief?name=<%= a.getNaam()%>">
			<fieldset>
				<select name="type">
					<option value="0">Lang niet geweest</option>
					<option value="1">Factuur</option>
					<option value="2">Openstande bestaling</option>
				</select><br>
				<select name="auto">
					<% for(Auto auto: a.getAutoLijst()){ %>
						<option value=<%= auto.getKenteken() %>><%= auto.getKenteken() %></option>
					<% } %>
				</select><br>
				<input type="submit" value="Verstuur">
			</fieldset>
		</form>
		<a href="klanten.jsp">Cancel</a>
	</div>
	<script src="http://code.jquery.com/jquery-2.1.3.js"></script>
	<script>
		$(function(){
			if($("select[name='auto'] option").length == 0){
				$("select[name='auto']").hide();
				$("select[name='auto']").parent().append("De klant heeft nog geen autos, voeg deze toe voor dat je brieven verstuurd");
			}
		});
	</script>
</body>
<html>