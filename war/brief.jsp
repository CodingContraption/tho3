<!DOCTYPE>
<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="klantenbinding.Brief" %>
	<% Brief b = (Brief)request.getAttribute("brief"); %>
	<div id="Menu">
		<ul>
			<li><a href='index.jsp'>Home</a></li>
		</ul>
	</div>
	<div id="Content">
		<span><%= b.getBody() %></span>
	</div>
</body>
<html>