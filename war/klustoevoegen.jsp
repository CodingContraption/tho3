<!DOCTYPE>
<html>
<head>
	<title>klus toevoegen</title>
	<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="bedrijf.Bedrijf"%>
	<%@ page import="klantenbinding.Klant"%>
	<%@ page import="werkplaats.Monteur"%>
	
	<%
		Bedrijf b = (Bedrijf) request.getServletContext().getAttribute("Bedrijf");
		b.nieuweKlant(new Klant("karel", "adres1", "adres@adres.nl", true));
		b.nieuweKlant(new Klant("henk", "adres1", "adres@adres.nl", true));
	%>

	<div id="Menu">
		<form name="addKlusEen" method="get" action="klustoevoegenKlant">
			<select name="type">
				<option value="APK">APK</option>
				<option value="reparatie">reparatie</option>
				<option value="tanken">tanken</option>
			</select> 
			
			<select name="monteur">
				<%
					for (Monteur m : b.getMonteurs()) {
				%>
				<option value=<%=m.getMonteurID()%>> <%=m.getNaam()%> </option>
				<% } %>
			</select> 
			
			<select name="klant">
				<%
					for (Klant k : b.getKlanten()) {
				%>
				<option value=<%=k.getNaam()%>> <%=k.getNaam()%> </option>
				<% } %>
			</select> 
			
			<input type="hidden" name="weeknr" value=<%=request.getParameter("weeknr")%> /> 
			<input type="hidden" name="jaar" value=<%=request.getParameter("jaar")%> /> 
			<input type="hidden" name="dag" value=<%=request.getParameter("dag")%> /> 
			<input type="hidden" name="tijd" value=<%=request.getParameter("tijd")%> /> 
			<input type="submit" value="volgende" />
		</form>
	</div>
</body>
<html>