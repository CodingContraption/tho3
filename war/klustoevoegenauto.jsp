<!DOCTYPE>
<html>
<head>
	<title>klus toevoegen</title>
	<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="bedrijf.Bedrijf"%>
	<%@ page import="klantenbinding.Klant"%>
	<%@ page import="klantenbinding.Auto"%>
	<%@ page import="java.util.Calendar"%>
	
	<%
	Bedrijf b = (Bedrijf) request.getServletContext().getAttribute("Bedrijf");
	b.nieuweKlant(new Klant("karel", "adres1", "adres@adres.nl", true));
	b.nieuweKlant(new Klant("henk", "adres1", "adres@adres.nl", true));
	%>

	<div id="Menu">
		<form name="addKlusTwee" method="post" action="klusaanmaken">
			<select name="auto">
				<%
					Klant k = (Klant) request.getAttribute("klant");
					k.voegAutoToe(new Auto(Calendar.getInstance(), Calendar.getInstance(), "12-dsf-1", "Audi", "Neongroen"));
					for (Auto a : k.getAutoLijst()) {
				%>
				<option value=<%=a.getKenteken() %>><%=a.getMerk() %> - <%=a.getKleur() %> - <%=a.getKenteken()%></option>
				<%  } %>
			</select> 
			<input type="hidden" name="type" value=<%=request.getAttribute("type")%> /> 
			<input type="hidden" name="klant" value=<%=k.getNaam()%> />
			<input type="hidden" name="monteurid" value=<%=request.getAttribute("monteurid")%> />
			
			<input type="hidden" name="weeknr" value=<%=request.getAttribute("weeknr")%> /> 
			<input type="hidden" name="jaar" value=<%=request.getAttribute("jaar")%> /> 
			<input type="hidden" name="dag" value=<%=request.getAttribute("dag")%> /> 
			<input type="hidden" name="tijd" value=<%=request.getAttribute("tijd")%> /> 
			<input type="submit" value="opslaan" />
		</form>
	</div>
</body>
<html>