<!DOCTYPE>
<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="java.util.ArrayList" %>
	<%@ page import="klantenbinding.Auto" %>
	<%@ page import="klantenbinding.Klant" %>
	<div id="Menu">
		<ul>
			<li><a href='index.jsp'>Home</a></li>
			<li><a href='voegKlantToe.jsp'>Klant toevoegen</a></li>
		</ul>
	</div>
	<div id="Content">
		<table>
		<tr>
		<th>Naam</th><th>Adres</th><th>E-Mail</th><th>Vaste klant?</th><th>Autos</th>
		</tr>
		<% 
		ArrayList<Klant> klanten = (ArrayList<Klant>)request.getAttribute("klanten"); 
		if(klanten.size() > 0){
			for(Klant a: klanten) { %>
				<tr>
						<td><a href="Klant?name=<%= a.getNaam() %>"><%= a.getNaam() %></a></td>
						<td><%= a.getAdres() %></td>
						<td><%= a.getEmail() %></td>
						<td><%= a.getVasteKlant() %></td>
						<td><% for(Auto auto: a.getAutoLijst()){ %>	
							<%= auto.getKenteken() %><br>
							<%= auto.getMerk() %><br>
							<%= auto.getKleur() %><br>
							<% } %>
						</td>				 
				</tr>
			<% } 
		}else{%>
			<tr>
				<td>Geen Klanten gevonden</td>
			</tr>
		<% } %> 
		</table>
		<form method="POST" action="KlantSearch">
			<input type="text" name="name">
			<input type="submit" value="Zoek">		
		</form>
	</div>
</body>
<html>