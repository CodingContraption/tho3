<!DOCTYPE>
<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="java.util.ArrayList" %>
	<%@ page import="java.util.HashMap" %>
	<%@ page import="java.util.Calendar" %>
	
	<% 
		HashMap<Calendar, Integer[]> weekplanning = (HashMap<Calendar, Integer[]>)request.getAttribute("weekplanning"); 
		ArrayList<Calendar> dagenvdweek = (ArrayList<Calendar>)request.getAttribute("dagenvdweek");
		int weeknr = (int) request.getAttribute("weeknr");
        int jaar = (int) request.getAttribute("jaar");
	%>
	<div id="Menu">
		<ul>
			<li><a href="index.jsp">Home</a></li>
		</ul>
	</div>
	<div id="Content">
		<table>
			<thead>
				<tr>
					<th>Tijd</th>
					<th>Maandag</th>
					<th>Dinsdag</th>
					<th>Woensdag</th>
					<th>Donderdag</th>
					<th>Vrijdag</th>
				</tr>
			</thead>
			<% 
				Integer[] maandag = new Integer[4];
				Integer[] dinsdag = new Integer[4];
				Integer[] woensdag = new Integer[4];
				Integer[] donderdag = new Integer[4];
				Integer[] vrijdag = new Integer[4];
				
				%><h1>
                <%= jaar %>
                 - Week <%= weeknr %>
					<%
						if(weeknr == Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)) {%>
							(Current week)
						<%}
					%>
				</h1><%
				
				for(Calendar cal : dagenvdweek) {
					Integer[] klussen = weekplanning.get(cal);
					if((cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) && (weekplanning.get(cal) != null)) {
						maandag = weekplanning.get(cal);
					} else if((cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) && (weekplanning.get(cal) != null)) {
						dinsdag = weekplanning.get(cal);
					} else if((cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) && (weekplanning.get(cal) != null)) {
						woensdag = weekplanning.get(cal);
					} else if((cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) && (weekplanning.get(cal) != null)) {
						donderdag = weekplanning.get(cal);
					} else if((cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) && (weekplanning.get(cal) != null)) {
						vrijdag = weekplanning.get(cal);
					}
				}
				
				String klusmaken = "<a href='klustoevoegen.jsp?weeknr="+weeknr;
				String jaarstring = "&jaar=";
				String dag = "&dag=";
				String tijd = "&tijd=";
				String klusmaken2 = "'>Nieuwe klus</a>";
				
				String klusinfo = "<a href='klusinfo.jsp?klusid=";
				String klusinfo2 = "'>Toon klusinfo<a/>";
			%>
			<tbody>
			<% for(int i = 0; i < 4; i++) { %>
				<tr>
					<td>
						<%
						String string = "";
						switch (i) {
						case 0:  string = "10:00 - 12:00";
								 break;
			            case 1:  string = "12:00 - 14:00";
			                     break;
			            case 2:  string = "14:00 - 16:00";
			                     break;
			            case 3:  string = "16:00 - 18:00";
	                    		 break;
						}
						%>
						<%= string %>
					</td>
					<!-- ma -->
					<td><% if(maandag[i] == null) {%>
						<%= klusmaken + jaarstring + jaar + dag + Calendar.MONDAY + tijd + i + klusmaken2 %>
					<%} else {%>
						<%= klusinfo + maandag[i] + klusinfo2 %>
					<% } %></td>
					
					<!-- di -->
					<td><% if(dinsdag[i] == null) {%>
						<%= klusmaken + jaarstring + jaar + dag + Calendar.TUESDAY + tijd + i + klusmaken2 %>
					<%} else {%>
						<%= klusinfo + dinsdag[i] + klusinfo2 %>
					<% } %></td>
					
					<!-- wo -->
					<td><% if(woensdag[i] == null) {%>
						<%= klusmaken + jaarstring + jaar + dag + Calendar.WEDNESDAY + tijd + i + klusmaken2 %>
					<%} else {%>
						<%= klusinfo + woensdag[i] + klusinfo2 %>
					<% } %></td>
					
					<!-- do -->
					<td><% if(donderdag[i] == null) {%>
						<%= klusmaken + jaarstring + jaar + dag + Calendar.THURSDAY + tijd + i + klusmaken2 %>
					<%} else {%>
						<%= klusinfo + donderdag[i] + klusinfo2 %>
					<% } %></td>
					
					<!-- vr -->
					<td><% if(vrijdag[i] == null) {%>
						<%= klusmaken + jaarstring + jaar + dag + Calendar.FRIDAY + tijd + i + klusmaken2 %>
					<%} else {%>
						<%= klusinfo + vrijdag[i] + klusinfo2 %>
					<% } %></td>
				</tr>
			<% } %>
			</tbody>
		</table>
		<a href="planning?iteration=prev&weeknr=<%= weeknr %><% if(jaar != Calendar.getInstance().get(Calendar.YEAR)){%>&jaar=<%= jaar %><%}%>"><button>Previous week</button></a>
        <a href="planning?iteration=next&weeknr=<%= weeknr %><% if(jaar != Calendar.getInstance().get(Calendar.YEAR)){%>&jaar=<%= jaar %><%}%>"><button>Next week</button></a>

	</div>
</body>
<html>