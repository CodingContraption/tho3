<!DOCTYPE>

<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="java.util.ArrayList" %>
	<%@ page import="java.util.HashMap" %>
	<%@ page import="java.util.Calendar" %>
	<%@ page import="bedrijf.Bedrijf"%>
	<%@ page import="werkplaats.Klus"%>
	<%@ page import="voorraadbeheer.Artikel"%>
	<%@ page import="voorraadbeheer.Brandstof"%>
	<%@ page import="voorraadbeheer.Onderdeel"%>

	<% 
		Bedrijf bedrijf = (Bedrijf) application.getAttribute("Bedrijf");
		int klusid = Integer.parseInt(request.getParameter("klusid"));
		Klus klus = bedrijf.getPlanning().getKlusById(klusid);
	%>
	<div id="Menu">
		<ul>
			<li><a href='index.jsp'>Home</a></li>
			<li><a href='planning'>Planning</a></li>
		</ul>
	</div>
	<div id="Content">
		Type klus: <%= klus.getType() %><br>
		Betreffende klant: <%= klus.getKlant().getNaam() %><br>
		Betreffende auto: <%= klus.getBetrokkenAuto().getKenteken() %><br>
		<br><br>
		Monteur: <%= klus.getBetrokkenMonteur().getNaam() %><br>
		<br><br>
		gebruikte onderdelen:<br>
		
		<table>
			<thead>
				<tr>
					<th>Naam</th>
					<th>Type</th>
				</tr>
			</thead>
			<tbody>
			<% for(Artikel art : klus.getGebruikteArtikelen()) {
				%>
				<tr>
					<td><%= art.getName() %></td>
					<td><%= art.getArtikelType() %></td>
				</tr>
			<%}%>
			</tbody>
		</table>
		
		<form name="addKlusTwee" method="post" action="klusbijwerken">
			<select name="artikel">
				<%
					for (Artikel art2 : bedrijf.getCatalogus().getAllArtikelen()) {
				%>
				<option value=<%=art2.getName()%>> <%=art2.getName()%> </option>
				<% } %>
			</select> 
			<input type="number" name="aantal" step="0.1" />
			<input name="klusid" type="hidden" value="<%= klusid %>" />
			<input type="submit" value="Bijwerken" />
		</form>
		
		<a href="Factuur?klusid=<%= klusid %>"><button>Factureren</button></a>
	</div>
</body>
<html>