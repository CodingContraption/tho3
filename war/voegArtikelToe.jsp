<!DOCTYPE>
<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<div id="Menu">
		<ul>
			<li><a href='index.jsp'>Home</a></li>
			<li><a href='Catalogus'>Terug naar Catalogus</a></li>
		</ul>
	</div>
	<div id="Content">
		<form method="POST" action="addArtikel">
		<fieldset>
		<select name="type">
			<option value="Brandstof" selected>Brandstof</option>
			<option value="Onderdeel">Onderdeel</option>
		</select><br>
		<input type="text" name="name" placeholder="Vul hier de naam van het Artikel in" />
		</fieldset>
		<fieldset id="brandstof" >
			<input type="text" name="BrandstofType" placeholder="Vul hier de brandstof type in" /><br>
			<input type="number" name="liter" min="1" placeholder="Vul hier het aantal liter in" /><br>
			<input type="number" name="minL" min="1" placeholder="Vul hier het minimum aantal liter in" /><br>
			<input type="text" name="TSIC" placeholder="Vul hier het tankStationIdentificatieCode in" /><br>
			<input type="number" name="pPL" min="0" step="0.1" placeholder="Vul hier de prijs per liter in" />
		</fieldset>
		<fieldset id="onderdeel">
			<input type="number" name="nummer" placeholder="Vul hier het Onderdeel Nummer in" /><br>
			<input type="number" name="aantal" min="1" placeholder="Vul hier het aantal in" /><br>
			<input type="number" name="minA" min="1" placeholder="Vul hier het minimum aantal in" /><br>
			<input type="number" name="pPS" min="0" step="0.1" placeholder="Vul hier de prijs per stuk in" />
		</fieldset>
		<input type="submit" value="Voeg Toe" />
		</form>
	</div>
	<script src="http://code.jquery.com/jquery-2.1.3.js"></script>
	<script>
		$(function(){
			$("#brandstof").show();	   
			$("#onderdeel").hide();
			$("select").on("change", function(){
				if($("select").val()=="Onderdeel"){
					$("#brandstof").hide();	
					$("#onderdeel").show();
				}else{
					$("#brandstof").show();	   
					$("#onderdeel").hide();
				}
			});
		});
	</script>
</body>
<html>