<!DOCTYPE>
<html>
<head>
<title>Homepage</title>
<link rel="stylesheet" type="text/css" href="style-home.css" />
</head>
<body>
	<%@ page import="voorraadbeheer.Artikel" %>
	<%@ page import="voorraadbeheer.Onderdeel" %>
	<%@ page import="voorraadbeheer.Brandstof" %>
	<div id="Menu">
		<ul>
			<li><a href='index.jsp'>Home</a></li>
			<li><a href='Catalogus'>Terug naar catalogus</a></li>
		</ul>
	</div>
	<div id="Content">
	<% Artikel a = (Artikel)request.getAttribute("artikel"); %>
	<form method="POST" action="EditArtikel?name=<%= a.getName() %>">
			<fieldset>
			<input type="hidden" name="type" value="<%= a.getArtikelType() %>" />
			<input type="text" name="naam" value="<%= a.getName() %>" placeholder="Vul hier de naam van het Artikel in" />
			</fieldset>
		
		<% if(a.getArtikelType().equals("Brandstof")){
				Brandstof b = (Brandstof) a; %>
				
			<fieldset id="brandstof" >
				<input type="text" name="BrandstofType" value="<%= b.getBrandstofType() %>" placeholder="Vul hier de brandstof type in" /><br>
				<input type="number" name="liter" min="1" value="<%= b.getLiter() %>" placeholder="Vul hier het aantal liter in" /><br>
				<input type="number" name="minL" min="1" value="<%= b.getMinLiter() %>" placeholder="Vul hier het minimum aantal liter in" /><br>
				<input type="text" name="TSIC" value="<%= b.getTankStationIdentificatieCode() %>" placeholder="Vul hier het tankStationIdentificatieCode in" /><br>
				<input type="number" name="pPL" value="<%= b.getPrijsPerLiter() %>" min="0" step="0.1" placeholder="Vul hier de prijs per liter in" />
			</fieldset>
			
			<%}else{
				Onderdeel o = (Onderdeel) a;%>
				
				<fieldset id="onderdeel">
				<input type="number" name="nummer" value="<%= o.getNummer() %>" placeholder="Vul hier het Onderdeel Nummer in" /><br>
				<input type="number" name="aantal" value="<%= o.getAantal() %>" min="1" placeholder="Vul hier het aantal in" /><br>
				<input type="number" name="minA" value="<%= o.getMinAantal() %>" min="1" placeholder="Vul hier het minimum aantal in" /><br>
				<input type="number" name="pPS" value="<%= o.getPrijsPerStuk() %>" min="0" step="0.1" placeholder="Vul hier de prijs per stuk in" />
			</fieldset>
			
			<% } %>
			
			<input type="submit" value="Edit" />
		</form>	
		<a href="Catalogus">Cancel</a>
	</div>
</body>
<html>