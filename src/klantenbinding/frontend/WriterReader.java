package klantenbinding.frontend;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import klantenbinding.Brief;
import klantenbinding.Klant;

public class WriterReader implements Serializable { // klasse bedoeld om de
	private static final long serialVersionUID = 2036917750540028478L;
	// brieven
	// uit txt file te lezen en te
	// schrijven.
	ArrayList<Brief> brieven;
	ArrayList<Klant> klanten;
	public WriterReader() {

	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList haalBestandenOp(String type) {

		FileInputStream fis;
		try {
			fis = new FileInputStream("d://data/" + type + ".txt");
			ObjectInputStream ois = new ObjectInputStream(fis);
			brieven = (ArrayList<Brief>) ois.readObject();

			ois.close();
		} catch (FileNotFoundException e) {
			brieven = new ArrayList<Brief>();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return (ArrayList) brieven;

	}
																															
	public void slaBrievenOp(ArrayList<Brief> brieven) {

		@SuppressWarnings("unused")
		boolean dirFlag = false;
		File stockDir = new File("d://data");

		try {
		   dirFlag = stockDir.mkdir();
		} catch (SecurityException Se) {
		System.out.println("Error while creating directory in Java:" + Se);
		}
		
		
		try {
			FileOutputStream fos = new FileOutputStream("d://data/brieven.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(brieven); // write MenuArray to ObjectOutputStream
			oos.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			
			
		}

		

	}

	public void slaKlantenOp(ArrayList<Klant> klanten) {
		try {
			FileOutputStream fos = new FileOutputStream("d://data/klanten.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(klanten); // write MenuArray to ObjectOutputStream
			oos.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void nieuweBrief(Brief nieuweBrief) {

		brieven = this.haalBestandenOp("brieven");

		if (!this.briefBestaat(nieuweBrief)) {
			brieven.add(nieuweBrief);
		}

		this.slaBrievenOp(brieven);

	}
	
	@SuppressWarnings("unchecked")
	public void nieuweKlant(Klant nieuweKlant) {
		
		klanten = this.haalBestandenOp("klanten");
		
		if (!this.klantBestaat(nieuweKlant)) {
			klanten.add(nieuweKlant);
		}

		this.slaKlantenOp(klanten);

	}
	
	

	public Boolean briefBestaat(Brief brief) {
		this.haalBestandenOp("brieven");
		boolean bestaatAl = false;
		for (Brief lijstBrief : this.brieven) {
			if (brief.equals(lijstBrief)) {
				bestaatAl = true;
			}
		}

		return bestaatAl;
	}
	
	@SuppressWarnings("unchecked")
	public Boolean klantBestaat(Klant klant) {
		this.haalBestandenOp("klanten");
		boolean bestaatAl = false;
		for (Klant lijstKlant : (ArrayList<Klant>)this.haalBestandenOp("klanten")) {
			if (klant.equals(lijstKlant)) {
				bestaatAl = true;
			}
		}

		return bestaatAl;
	}
	
}
