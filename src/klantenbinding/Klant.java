package klantenbinding;
import java.io.Serializable;
import java.util.ArrayList;

public class Klant  implements Serializable{
	private static final long serialVersionUID = -6782535976087922998L;
	private String naam;
	private String adres;
	private String email;
	// klant ID weggehaald, omdat niet nodig is, moet nog uit klassediagram
	// worden gehaald
	private Boolean vasteKlant;
	private ArrayList<Auto> autoLijst;
	
	public Klant(String nm, String adres, String email, boolean vasteKlant) { // bij
																				// de
																				// constructor,
																				// word
																				// er
																				// nog
																				// GEEN
																				// auto
																				// toegevoegd,
																				// auto's
																				// worden
																				// toegevoegd
																				// aan
																				// een
																				// klant
																				// bij
																				// het
																				// maken
																				// van
																				// een
																				// auto
		this.setNaam(nm);
		this.setAdres(adres);
		this.setEmail(email);
		this.setVasteKlant(vasteKlant); // lijkt me in de UI het mooist met een
										// checkbox
		this.setAutoLijst(new ArrayList<Auto>()); // initialiseert de ArrayList
													// voor auto's in de
													// constructor
		
		
	}

	// de getters & setters...

	private void setAutoLijst(ArrayList<Auto> autoLijst) { // private omdat deze
															// functie nooit
															// aangeroepen hoeft
															// te worden,
															// behalve tijdens
															// de constructor
		this.autoLijst = autoLijst;
	}

	public ArrayList<Auto> getAutoLijst() {
		return this.autoLijst;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getVasteKlant() {
		return vasteKlant;
	}

	public void setVasteKlant(Boolean vasteKlant) {
		this.vasteKlant = vasteKlant;
	}

	// .. einde getters & setters

	// functie om auto toe te voegen bij de klant

	public void voegAutoToe(Auto auto) {
		if (!this.heeftAuto(auto)) {
			autoLijst.add(auto);
		}
		// moet iets van een error komen, weet zo snel even niet hoe best op te
		// lossen **

	}

	// 2 methodes om een auto te zoeken, 1 met behulp van een autoklasse, en 1
	// met behulp van een kenteken
	public Auto zoekAuto(Auto auto) {
		Auto returnAuto = null;
		for (Auto gevondenAuto : this.autoLijst) {
			if (gevondenAuto.getKenteken().equals(auto.getKenteken())) {
				returnAuto = gevondenAuto;
			}
		}
		return returnAuto;
	}

	public Auto zoekAuto(String kenteken) {
		Auto returnAuto = null;
		for (Auto gevondenAuto : this.autoLijst) {
			if (gevondenAuto.getKenteken().equals(kenteken)) {
				returnAuto = gevondenAuto;
			}
		}
		return returnAuto;
	}

	// 2 methodes om te checken of de klant de auto heeft, 1 met behulp van een
	// autoklasse, en 1 met behulp van een kenteken
	public Boolean heeftAuto(Auto auto) {
		Boolean heeftAuto = false;
		for (Auto gevondenAuto : this.autoLijst) {
			if (gevondenAuto.getKenteken().equals(auto.getKenteken())) {
				heeftAuto = true;
			}
		}
		return heeftAuto;
	}

	public Boolean heeftAuto(String kenteken) {
		Boolean heeftAuto = false;
		for (Auto gevondenAuto : this.autoLijst) {
			if (gevondenAuto.getKenteken().equals(kenteken)) {
				heeftAuto = true;
			}
		}
		return heeftAuto;
	}

	// een toString functie
	public String toString() {
		String text = "naam: " + this.getNaam() + " || adres: " + this.getAdres()
				+ " || Email: " + this.getEmail() + " || vasteklant: "
				+ this.getVasteKlant() + " || met de auto's: <br>";
		for(Auto a: this.getAutoLijst()) {
			text += a.getKenteken() + "<br>";
		}
		
		return text;
	}

	public Boolean equals(Klant klant){
		Boolean returnb = false;
		if (klant.getVasteKlant().equals(this.getVasteKlant()) && klant.getAdres().equals(this.getAdres()) && klant.getEmail().equals(this.getEmail()) && klant.getNaam().equals(this.getNaam())){
			returnb = true;
		}
		return returnb;
	}
}
