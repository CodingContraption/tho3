package klantenbinding.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import klantenbinding.Klant;
import bedrijf.Bedrijf;

public class KlantSeach extends HttpServlet{
	private static final long serialVersionUID = -5232161029409339185L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String Klant = req.getParameter("name");
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");

		ArrayList<Klant> temp = b.search(Klant);
		req.setAttribute("klanten", temp);
		
		rd = req.getRequestDispatcher("klanten.jsp");
		rd.forward(req,res);		
	}
}
