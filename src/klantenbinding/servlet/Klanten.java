package klantenbinding.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import klantenbinding.Klant;
import bedrijf.Bedrijf;

public class Klanten extends HttpServlet{
	private static final long serialVersionUID = 5827006362389567863L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf) req.getServletContext().getAttribute("Bedrijf");
		ArrayList<Klant> klanten = b.getKlanten();
		
		req.setAttribute("klanten", klanten);
		
		rd = req.getRequestDispatcher("klanten.jsp");
		
		rd.forward(req, res);
	}
	
}
