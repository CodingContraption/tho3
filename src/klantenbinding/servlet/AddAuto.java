package klantenbinding.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import klantenbinding.Auto;
import klantenbinding.Klant;
import bedrijf.Bedrijf;

public class AddAuto extends HttpServlet{
	private static final long serialVersionUID = -573634981240771848L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		String naam = req.getParameter("Klantnaam");
		String kenteken = req.getParameter("kenteken");
		String merk = req.getParameter("merk");
		String kleur = req.getParameter("kleur");
		
		Calendar cal = Calendar.getInstance();
		
		Auto a = new Auto(cal, cal, kenteken, merk, kleur);
		
		ArrayList<Klant> klanten = b.getKlanten();
		for(Klant k: klanten){
			if(k.getNaam().toLowerCase().equals(naam.toLowerCase())){
				k.voegAutoToe(a);
			}
		}
		
		req.setAttribute("klanten", klanten);
		
		rd = req.getRequestDispatcher("klanten.jsp");
		rd.forward(req,res);
	}
	
}