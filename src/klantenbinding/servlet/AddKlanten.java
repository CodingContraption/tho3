package klantenbinding.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import klantenbinding.Klant;
import bedrijf.Bedrijf;

public class AddKlanten extends HttpServlet{
	private static final long serialVersionUID = -4030763154592761291L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		String naam = req.getParameter("naam");
		String adres = req.getParameter("adres");
		String email = req.getParameter("email");
		Boolean vasteklant = Boolean.parseBoolean(req.getParameter("vasteklant"));
		
		Klant nieuweKlant = new Klant(naam, adres, email, vasteklant);
		
		b.nieuweKlant(nieuweKlant);
		
		ArrayList<Klant> klanten = b.getKlanten();
		
		req.setAttribute("klanten", klanten);
		
		rd = req.getRequestDispatcher("klanten.jsp");
		rd.forward(req,res);
	}
	
}