package klantenbinding.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import klantenbinding.Klant;
import bedrijf.Bedrijf;

public class GetKlant extends HttpServlet{
	private static final long serialVersionUID = 3338106762960201609L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String Klantname = req.getParameter("name");
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		Klant k = b.getKlant(Klantname);
		
		req.setAttribute("klant", k);
		
		rd = req.getRequestDispatcher("klant.jsp");
		rd.forward(req,res);		
	}

}