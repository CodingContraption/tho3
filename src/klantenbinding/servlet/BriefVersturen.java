package klantenbinding.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import klantenbinding.Auto;
import klantenbinding.Brief;
import klantenbinding.Klant;
import bedrijf.Bedrijf;

public class BriefVersturen extends HttpServlet{
	private static final long serialVersionUID = -8141651511946292776L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		String naam = req.getParameter("name");
		int type = Integer.parseInt(req.getParameter("type"));
		String auto = req.getParameter("auto");
		
		Klant klant = null;
		Auto a = null;
		
		ArrayList<Klant> klanten = b.getKlanten();
		for(Klant k: klanten){
			if(k.getNaam().toLowerCase().equals(naam.toLowerCase())){
				klant = k;
			}
		}
		for(Auto au: klant.getAutoLijst()){
			if(au.getKenteken().toLowerCase().equals(auto.toLowerCase())){
				a = au;
			}
		}
		Calendar cal = Calendar.getInstance();
		Brief brief = new Brief(type, klant, a, cal);
		
		req.setAttribute("brief", brief);
		
		rd = req.getRequestDispatcher("brief.jsp");
		rd.forward(req,res);
	}
	
}