package klantenbinding;
import java.io.Serializable;
import java.util.Calendar;


public class Auto  implements Serializable{
	private static final long serialVersionUID = 5208184036723083819L;
	private Calendar laatsteBezoek;
	private Calendar laatsteAPK;
	private String kenteken;
	private String merk;
	private String kleur;
	private Boolean BriefVerstuurd;
	
	
	public Auto(Calendar laatsteBezoek, Calendar laatsteAPK, String kenteken,
			String merk, String kleur) {
		this.setLaatsteBezoek(laatsteBezoek);
		this.setLaatsteAPK(laatsteAPK);
		this.setKenteken(kenteken);
		this.setMerk(merk);
		this.setKleur(kleur);
		
		
	}
	public void setBriefVerstuurd(boolean b) {
		this.BriefVerstuurd = b;
		
	}
	
	public Boolean getBriefVerstuurd(){
		return this.BriefVerstuurd;
	}
	public Calendar getLaatsteBezoek() {
		return laatsteBezoek;
	}
	public void setLaatsteBezoek(Calendar laatsteBezoek) {
		this.laatsteBezoek = laatsteBezoek;
		setBriefVerstuurd(false);
		
	}
	public Calendar getLaatsteAPK() {
		return laatsteAPK;
	}
	public void setLaatsteAPK(Calendar laatsteAPK) {
		this.laatsteAPK = laatsteAPK;
	}
	public String getKenteken() {
		return kenteken;
	}
	public void setKenteken(String kenteken) {
		this.kenteken = kenteken;
	}
	public String getMerk() {
		return merk;
	}
	public void setMerk(String merk) {
		this.merk = merk;
	}
	public String getKleur() {
		return kleur;
	}
	public void setKleur(String kleur) {
		this.kleur = kleur;
	}
	
	public Boolean equals(Auto auto){
		Boolean returnb = false;
		if (auto.getKenteken().equals(this.getKenteken())){
			returnb = true;
		}
		return returnb;
	}
	

	
}
