package klantenbinding;

import java.io.Serializable;
import java.util.Calendar;

import klantenbinding.frontend.WriterReader;

public class Brief implements Serializable {
	private static final long serialVersionUID = -1451744525627560155L;
	private int type; // 0 = herrinering, 1 = factuur, 2 = aanmaning
	private String body;
	private Klant klant;
	private Auto auto;
	private WriterReader brieven = new WriterReader();
	private Calendar briefAangemaaktVoor; // 
	
	public Calendar getBriefAangemaaktVoor() {
		return briefAangemaaktVoor;
	}

	public void setBriefAangemaaktVoor(Calendar briefAangemaakt) {
		this.briefAangemaaktVoor = briefAangemaakt;
	}

	public Brief(int type, Klant klant, Auto auto, Calendar aangemaaktVoor) {
		if (klant.heeftAuto(auto)) {
			this.setAuto(auto);
			this.setType(type);
			this.setKlant(klant);
			this.briefAangemaaktVoor = aangemaaktVoor;
			if (this.getType() == 0) {
				this.setBody("Geachte heer/mevrouw "
						+ this.getKlant().getNaam()
						+ ", \n het is ons opgevallen dat er al een langere tijd niets van U is waargenomen, en vandaar deze melding, het gaat om de "
						+ this.getAuto().getMerk() + " met het kenteken: "
						+ this.getAuto().getKenteken());
			} else if (this.getType() == 1) {
				this.setBody("Geachte heer/mevrouw "
						+ this.getKlant().getNaam()
						+ ", \n U bent bij ons geweest, en hierbij de factuur van de auto  "
						+ this.getAuto().getMerk() + " met het kenteken: "
						+ this.getAuto().getKenteken());
			} else {
				this.setBody("Geachte heer/mevrouw "
						+ this.getKlant().getNaam()
						+ ", \n U heeft achterstallige betalingen, het gaat hier om de auto "
						+ this.getAuto().getMerk() + " met het kenteken: "
						+ this.getAuto().getKenteken());

			}

		}

		else {
			// error, klant auto combinatie komt NIET overheen, ergens gaat iets
			// fout in programma
		}
		brieven.nieuweBrief(this);

	}

	public Auto getAuto() {
		return auto;
	}

	public void setAuto(Auto auto) {
		this.auto = auto;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Klant getKlant() {
		return klant;
	}

	public void setKlant(Klant klant) {
		this.klant = klant;
	}

	// een toString functie
	public String toString() {
		String type;
		if (this.getType() == 0) {
			type = "herinnering";
		} else if (this.getType() == 1) {
			type = "aanmaning";
		}

		else if (this.getType() == 2) {
			type = "factuur";
		} else {
			type = "onbekend"; // dit zou in theorie nooit moeten voorkomen,
								// tenzij een fout in de code zit
								// in de UI hoeft namelijk nooit handmatig een
								// string"nummer" te worden aangegeven.
		}

		return "ontvanger: " + this.getKlant().getNaam() + ", bericht: "
				+ this.getBody() + ", type: " + type;
	}

	public boolean equals(Brief brief) {
		Boolean returnb = false;
		if (brief.getAuto().equals(this.getAuto()) && brief.getBody().equals(this.getBody()) && brief.getKlant().equals(this.getKlant()) && brief.getType()==this.getType() ){
			returnb = true;
		}
		return returnb;
	}
}
