package voorraadbeheer.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bedrijf.Bedrijf;
import voorraadbeheer.Artikel;
import voorraadbeheer.Catalogus;

public class Search extends HttpServlet{
	private static final long serialVersionUID = -6091928315796662498L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String Artikelname = req.getParameter("name");
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		Catalogus cat = b.getCatalogus();
		ArrayList<Artikel> temp = cat.searchArtikelen(Artikelname);
		req.setAttribute("contents", temp);
		
		rd = req.getRequestDispatcher("catalogus.jsp");
		rd.forward(req,res);		
	}
}
