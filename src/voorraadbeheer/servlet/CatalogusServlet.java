package voorraadbeheer.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import voorraadbeheer.Artikel;
import voorraadbeheer.Catalogus;
import bedrijf.Bedrijf;

public class CatalogusServlet extends HttpServlet{
	private static final long serialVersionUID = 8972485336438300895L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		Catalogus cat = b.getCatalogus();
		
		ArrayList<Artikel> artikelen = cat.getAllArtikelen();
		
		req.setAttribute("contents", artikelen);
		
		rd = req.getRequestDispatcher("catalogus.jsp");
		rd.forward(req,res);
	}

}
