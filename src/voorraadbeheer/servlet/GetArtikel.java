package voorraadbeheer.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bedrijf.Bedrijf;
import voorraadbeheer.Artikel;
import voorraadbeheer.Catalogus;

public class GetArtikel extends HttpServlet{
	private static final long serialVersionUID = 308643833796704079L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		String Artikelname = req.getParameter("name");
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		Catalogus cat = b.getCatalogus();
		Artikel a = cat.getArtikel(Artikelname);
		
		req.setAttribute("artikel", a);
		
		rd = req.getRequestDispatcher("artikel.jsp");
		rd.forward(req,res);		
	}

}
