package voorraadbeheer.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bedrijf.Bedrijf;
import voorraadbeheer.Artikel;
import voorraadbeheer.Catalogus;

public class EditArtikel extends HttpServlet{
	private static final long serialVersionUID = -6318313769063165870L;

	public void doPost(HttpServletRequest req, HttpServletResponse res)throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		Catalogus cat = b.getCatalogus();
		String Artikelname = req.getParameter("name");
		cat.removeArtikel(Artikelname);
		Artikelname = req.getParameter("naam");
		String type = req.getParameter("type");
		if(type.equals("Brandstof")){
			String brTyp = req.getParameter("BrandstofType");
			String TSIC = req.getParameter("TSIC");
			int liter = Integer.parseInt(req.getParameter("liter"));
			int minL = Integer.parseInt(req.getParameter("minL"));
			double pPL = Double.parseDouble(req.getParameter("pPL"));
			cat.addArtikelen(Artikelname, type, brTyp, liter, minL, TSIC, pPL);
		}else{
			int nummer = Integer.parseInt(req.getParameter("nummer"));
			int aantal = Integer.parseInt(req.getParameter("aantal"));
			int minA = Integer.parseInt(req.getParameter("minA"));
			double pPS = Double.parseDouble(req.getParameter("pPS"));
			cat.addArtikelen(Artikelname, type, nummer, minA, aantal, pPS);
		}
		ArrayList<Artikel> artikelen = cat.getAllArtikelen();
		
		req.setAttribute("contents", artikelen);
		
		rd = req.getRequestDispatcher("catalogus.jsp");
		
		rd.forward(req,res);
	}
	
}
