package voorraadbeheer.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bedrijf.Bedrijf;
import voorraadbeheer.Catalogus;

public class AddArtikel extends HttpServlet{
	private static final long serialVersionUID = 8771263819474523319L;

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		Catalogus cat = b.getCatalogus();
		
		req.setAttribute("contents", cat.getAllArtikelen());
		
		String type = req.getParameter("type");
		String name = req.getParameter("name");
		
		if(type.equals("Brandstof")){
			String brTyp = req.getParameter("BrandstofType");
			String TSIC = req.getParameter("TSIC");
			int liter = Integer.parseInt(req.getParameter("liter"));
			int minL = Integer.parseInt(req.getParameter("minL"));
			double pPL = Double.parseDouble(req.getParameter("pPL"));
			cat.addArtikelen(name, type, brTyp, liter, minL, TSIC, pPL);
		}else{
			int nummer = Integer.parseInt(req.getParameter("nummer"));
			int aantal = Integer.parseInt(req.getParameter("aantal"));
			int minA = Integer.parseInt(req.getParameter("minA"));
			double pPS = Double.parseDouble(req.getParameter("pPS"));
			cat.addArtikelen(name, type, nummer, minA, aantal, pPS);
		}
		
		rd = req.getRequestDispatcher("catalogus.jsp");
		rd.forward(req,res);
	}
	
}
