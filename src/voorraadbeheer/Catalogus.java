package voorraadbeheer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Catalogus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8392446392261860185L;
	private ArrayList<Artikel> artikelen = new ArrayList<Artikel>();
	
	public Catalogus() {
	}

	public ArrayList<Artikel> searchArtikelen(String query) {
		ArrayList<Artikel> tempList = new ArrayList<Artikel>();
		for(Artikel art : artikelen) {
			if(art.getName().toLowerCase().contains(query.toLowerCase())) {
				tempList.add(art);
			}
		} 
		return tempList;
	}

	public Artikel getArtikel(String name) {
		for(Artikel a: artikelen){
			if(a.getName().equals(name)){
				return a;
			}
		}
		return null;
	}
	
	public void removeArtikel(String name){
		for (int i=artikelen.size()-1; i> -1; i--) {
		    if (artikelen.get(i).getName().toLowerCase().equals(name.toLowerCase()) ) {
		    	artikelen.remove(i);
		    }
		}
	}
	
	public boolean addArtikelen(String name, String type, String brTyp, double liter, double minL, String TSIC, double pPL) {
		Artikel brandstof = new Brandstof(name, type, brTyp, liter, minL, TSIC, pPL);
		if(!artikelBestaatCheck(brandstof)) {
			artikelen.add(brandstof);
			return true;
		}
		return false;
	}
	
	public boolean addArtikelen(String name, String type, int nummer, int minA, int aantal, double pPS) {
		Artikel onderdeel = new Onderdeel(name, type, nummer, minA, aantal, pPS);
		if(!artikelBestaatCheck(onderdeel)) {
			artikelen.add(onderdeel);
			return true;
		}
		return false;
	}
	
	public boolean artikelBestaatCheck(Artikel art) {
		@SuppressWarnings("rawtypes")
		Iterator it = artikelen.iterator();
		while(it.hasNext()){
			if(it.next().equals(art)){
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<Artikel> getAllArtikelen(){
		return artikelen;
	}
	
	public String toString(){
		String s = "";
		
		@SuppressWarnings("rawtypes")
		Iterator it = artikelen.iterator();
		while(it.hasNext()){
			s+=it.next()+"\n";
		}
		
		return s;
	}
}