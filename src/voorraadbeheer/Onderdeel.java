package voorraadbeheer;

import java.io.Serializable;

public class Onderdeel extends Artikel implements Serializable {
	private static final long serialVersionUID = -5998294343962088042L;
	private int minAantal;
	private int aantal;
	private int nummer;
	private double prijsPerStuk;
	
	public Onderdeel(String name, String type, int nummer,int minA, int aantal, double pPS) {
		super(name, type);
		this.nummer = nummer;
		minAantal = minA;
		this.aantal = aantal;
		prijsPerStuk = pPS;
	}

	public int getNummer() {
		return nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public int getMinAantal() {
		return this.minAantal;
	}

	public void setMinAantal(int minAantal) {
		this.minAantal = minAantal;
	}

	public int getAantal() {
		return this.aantal;
	}

	public void setAantal(int aantal) {
		this.aantal = aantal;
	}

	public double getPrijsPerStuk() {
		return this.prijsPerStuk;
	}

	public void setPrijsPerStuk(double prijsPerStuk) {
		this.prijsPerStuk = prijsPerStuk;
	}

	@Override
	public String toString() {
		return "Onderdeel [minAantal=" + minAantal + ", aantal=" + aantal
				+ ", nummer=" + nummer + ", prijsPerStuk=" + prijsPerStuk + "]";
	}

	@Override
	public boolean equals(Object obj){
		if(obj == this){
			return true;
		}
		if(obj == null || obj.getClass() != this.getClass()){
			return false;
		}
		
		Onderdeel o = (Onderdeel) obj;
		return nummer == o.nummer && minAantal == o.minAantal && aantal == o.aantal && prijsPerStuk == o.prijsPerStuk;
		
	}

}