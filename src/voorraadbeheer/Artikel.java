package voorraadbeheer;

import java.io.Serializable;

public abstract class Artikel implements Serializable{
	private static final long serialVersionUID = -6158774958710662275L;
	private String name;
	private String artikelType;

	public Artikel(String name, String type) {
		this.name = name;
		artikelType = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getArtikelType() {
		return artikelType;
	}

	public void setArtikelType(String artikelType) {
		this.artikelType = artikelType;
	}
	
}