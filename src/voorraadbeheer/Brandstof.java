package voorraadbeheer;

import java.io.Serializable;

public class Brandstof extends Artikel implements Serializable{
	private static final long serialVersionUID = -3676796819555718235L;
	private String brandstofType;
	private double liter;
	private double minLiter;
	private String tankStationIdentificatieCode;
	private double prijsPerLiter;

	public Brandstof(String name, String type, String brTyp, double liter, double minL, String TSIC, double pPL) {
		super(name, type);
		brandstofType = brTyp;
		this.liter = liter;
		minLiter = minL;
		tankStationIdentificatieCode = TSIC;
		prijsPerLiter = pPL;
	}
	
	public String getBrandstofType() {
		return this.brandstofType;
	}

	public void setBrandstofType(String brandstofType) {
		this.brandstofType = brandstofType;
	}

	public double getLiter() {
		return this.liter;
	}

	public void setLiter(double liter) {
		this.liter = liter;
	}

	public double getMinLiter() {
		return this.minLiter;
	}

	public void setMinLiter(double minLiter) {
		this.minLiter = minLiter;
	}

	public String getTankStationIdentificatieCode() {
		return this.tankStationIdentificatieCode;
	}

	public void setTankStationIdentificatieCode(String tankStationIdentificatieCode) {
		this.tankStationIdentificatieCode = tankStationIdentificatieCode;
	}

	public double getPrijsPerLiter() {
		return this.prijsPerLiter;
	}

	public void setPrijsPerLiter(double prijsPerLiter) {
		this.prijsPerLiter = prijsPerLiter;
	}
	
	@Override
	public String toString() {
		return "Brandstof [brandstofType=" + brandstofType + ", liter=" + liter
				+ ", minLiter=" + minLiter + ", tankStationIdentificatieCode="
				+ tankStationIdentificatieCode + ", prijsPerLiter="
				+ prijsPerLiter + "]";
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == this){
			return true;
		}
		if(obj == null || obj.getClass() != this.getClass()){
			return false;
		}
		
		Brandstof o = (Brandstof) obj;
		return brandstofType == o.brandstofType && liter == o.liter && minLiter == o.minLiter && tankStationIdentificatieCode == o.tankStationIdentificatieCode && prijsPerLiter == o.prijsPerLiter;
		
	}

}