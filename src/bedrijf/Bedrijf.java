package bedrijf;
import java.io.Serializable;
import java.util.ArrayList;

import klantenbinding.Brief;
import klantenbinding.Klant;
import voorraadbeheer.Catalogus;
import werkplaats.Monteur;
import werkplaats.Weekplanning;

public class Bedrijf implements Serializable{
	private static final long serialVersionUID = -3938696512809695116L;
	
	private Catalogus catalogus = new Catalogus();
	private ArrayList<Klant> klanten = new ArrayList<Klant>();
	private ArrayList<Brief> brieven = new ArrayList<Brief>();
	private ArrayList<Monteur> monteurs = new ArrayList<Monteur>();
	private Weekplanning planning = new Weekplanning();
	
	public Bedrijf(Catalogus catalogus, ArrayList<Klant> klanten, ArrayList<Brief> brieven) {
		this.catalogus = catalogus;
		this.klanten = klanten;
	}

	public ArrayList<Monteur> getMonteurs() {
		return monteurs;
	}

	public void setMonteurs(ArrayList<Monteur> monteurs) {
		this.monteurs = monteurs;
	}

	public Catalogus getCatalogus() {
		return catalogus;
	}

	public void setCatalogus(Catalogus catalogus) {
		this.catalogus = catalogus;
	}

	public ArrayList<Klant> getKlanten() {
		return klanten;
	}

	public void setKlanten(ArrayList<Klant> klanten) {
		this.klanten = klanten;
	}

	public ArrayList<Brief> getBrieven() {
		return brieven;
	}

	public void setBrieven(ArrayList<Brief> brieven) {
		this.brieven = brieven;
	}
	
	public Weekplanning getPlanning() {
		return planning;
	}

	public void setPlanning(Weekplanning planning) {
		this.planning = planning;
	}
	
	//handmatig geimport uit WriterReader
	
	public void nieuweKlant(Klant nieuweKlant) {
		
		if (!this.klantBestaat(nieuweKlant)) {
			klanten.add(nieuweKlant);
		}

	}
	
	public Boolean klantBestaat(Klant klant) {
		boolean bestaatAl = false;
		for (Klant lijstKlant : klanten) {
			if (klant.equals(lijstKlant)) {
				bestaatAl = true;
			}
		}

		return bestaatAl;
	}
	
	public Klant getKlant(String naam){
		for(Klant k: klanten){
			if(k.getNaam().toLowerCase().equals(naam.toLowerCase())){
				return k;
			}
		}
		return null;
	}
	
	public void removeKlant(String name){
		for (int i=klanten.size()-1; i> -1; i--) {
		    if (klanten.get(i).getNaam().toLowerCase().equals(name.toLowerCase()) ) {
		    	klanten.remove(i);
		    }
		}
	}
	
	public ArrayList<Klant> search(String query){
		ArrayList<Klant> temp = new ArrayList<Klant>();
		for(Klant k: klanten){
			if(k.getNaam().toLowerCase().contains(query.toLowerCase())){
				temp.add(k);
			}
		}
		return temp;
	}
}
