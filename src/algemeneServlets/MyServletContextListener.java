package algemeneServlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import klantenbinding.Klant;
import voorraadbeheer.Catalogus;
import werkplaats.Monteur;
import bedrijf.Bedrijf;
import klantenbinding.Brief;

public class MyServletContextListener implements ServletContextListener{

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		Bedrijf b = (Bedrijf)sce.getServletContext().getAttribute("Bedrijf");
		
		File file = new File(sce.getServletContext().getRealPath("/data"));
		if (Files.notExists(file.toPath())) {
			 file.mkdir();
		}
		
		writeObjectToFile(b, sce.getServletContext().getRealPath("/data/Bedrijf.dat"));
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setAttribute("Bedrijf", 
						(Bedrijf) getObjectFromFile(sce.getServletContext().getRealPath("/data/Bedrijf.dat")));
		
		if(null == sce.getServletContext().getAttribute("Bedrijf")) {// check voor first run
			sce.getServletContext().setAttribute("Bedrijf", new Bedrijf(new Catalogus(), new ArrayList<Klant>(), new ArrayList<Brief>()));
		}
		
		Bedrijf b = (Bedrijf)sce.getServletContext().getAttribute("Bedrijf");
		ArrayList<Monteur> monteurs = new ArrayList<Monteur>();
		monteurs.add(new Monteur("Lisanne Smets", 1));
		monteurs.add(new Monteur("Anneke Arends", 2));
		monteurs.add(new Monteur("Hendrikje De Ven", 3));
		b.setMonteurs(monteurs);
	}
	
	public Object getObjectFromFile(String path){
		Object o = null;
		try {
			File f = new File(path);
			System.out.println(f.getAbsolutePath());
			FileInputStream fin = new FileInputStream(f);
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   o = ois.readObject();
			   ois.close();
			fin.close();
		} catch (IOException | ClassNotFoundException e) {
			System.out.println("#####");
			System.out.println("#####");
			System.out.println("If this is the first run, ignore this error.");
			System.out.println("#####");
			
			e.printStackTrace();
			
			System.out.println("#####");
			System.out.println("#####");
		}
		return o;
	}
	
	public void writeObjectToFile(Object o, String path) {
		FileOutputStream fout;
		ObjectOutputStream oos;
		try {
			fout = new FileOutputStream(path);
				oos = new ObjectOutputStream(fout);
				oos.writeObject(o);
				oos.close();
			fout.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
