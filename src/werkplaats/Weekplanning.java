package werkplaats;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Weekplanning implements Serializable{
	private static final long serialVersionUID = 7998850819343541596L;
	
	private HashMap<Calendar, Integer[]> planning = new HashMap<Calendar, Integer[]>();//lijst met datum op de klusnummers
	private HashMap<Integer, Klus> klussen = new HashMap<Integer, Klus>();//lijst van alle klusnummers gekoppeld aan hun klus
	
	public Weekplanning(){}
	
	public HashMap<Integer, Klus> getKlussen() {
		return this.klussen;
	}
	
	public Klus getKlusById(int klusid){
		return klussen.get(klusid);
	}
	
	public int addKlus(Klus klus, int weeknr, int jaar, int dag, int tijd) {
		int klusid = klussen.size() + 1;
		while(klussen.get(klusid) != null) {//if the klussen size +1 exists
			klusid++;
		}
		klussen.put(klusid, klus);
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, dag);
		cal.set(Calendar.WEEK_OF_YEAR, weeknr);
		cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    cal.set(Calendar.MILLISECOND, 0);
	    cal.set(Calendar.YEAR, jaar);
		
		Integer[] klusnrlijst = planning.get(cal);
		if(klusnrlijst == null) {
			klusnrlijst = new Integer[4];
		}
		klusnrlijst[tijd] = klusid;
		planning.put(cal, klusnrlijst);
		return klusid;
	}
	
	public boolean editKlus(Klus klus, int klusnr) {
		if(klussen.get(klusnr) != null) {
			klussen.put(klusnr, klus);
			return true;
		} else {
			return false;
		}
	}
	
    public HashMap<Calendar, Integer[]> getWeekPlanning(int week, int jaar) {
		HashMap<Calendar, Integer[]> weekplanning = new HashMap<Calendar, Integer[]>();
        ArrayList<Calendar> dagen = getDagenWeek(week, jaar);

		
		for(Calendar cal : dagen) {
			weekplanning.put(cal, planning.get(cal));
		}
		
		return weekplanning;
	}
	
	public int getWeekNr(Calendar date){
		int week = date.get(Calendar.WEEK_OF_YEAR);
		return week;
	}
	
    public ArrayList<Calendar> getDagenWeek(int week, int jaar) {
		ArrayList<Calendar> dates = new ArrayList<Calendar>();
		
		Calendar[] cal = new Calendar[6];
		for(int i = 1; i <= 5; i++) {
			cal[i] = Calendar.getInstance();
			cal[i].set(Calendar.WEEK_OF_YEAR, week);
			cal[i].set(Calendar.HOUR_OF_DAY, 0);
		    cal[i].set(Calendar.MINUTE, 0);
		    cal[i].set(Calendar.SECOND, 0);
		    cal[i].set(Calendar.MILLISECOND, 0);
            cal[i].set(Calendar.YEAR, jaar);

		}
		
		cal[1].set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		dates.add(cal[1]);
		cal[2].set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
		dates.add(cal[2]);
		cal[3].set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
		dates.add(cal[3]);
		cal[4].set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
		dates.add(cal[4]);
		cal[5].set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
		dates.add(cal[5]);
		
		return dates;
	}
}
