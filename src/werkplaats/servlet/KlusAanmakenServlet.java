package werkplaats.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import voorraadbeheer.Artikel;
import werkplaats.Klus;
import werkplaats.Monteur;
import bedrijf.Bedrijf;
import klantenbinding.Auto;
import klantenbinding.Klant;

public class KlusAanmakenServlet extends HttpServlet{
	private static final long serialVersionUID = 7454240635625419862L;
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		
		Bedrijf b = (Bedrijf) req.getServletContext().getAttribute("Bedrijf");
		
		String type = req.getParameter("type");
		Klant klant = b.getKlant(req.getParameter("klant"));
		Auto auto = klant.zoekAuto(req.getParameter("auto"));
		
		int weeknr = Integer.parseInt(req.getParameter("weeknr"));
		int jaar = Integer.parseInt(req.getParameter("jaar"));
		int dag = Integer.parseInt(req.getParameter("dag"));
		int tijd = Integer.parseInt(req.getParameter("tijd"));
		
		Monteur monteur = null;
		for(Monteur m : b.getMonteurs()) {
			if(m.getMonteurID() == Integer.parseInt(req.getParameter("monteurid"))) {
				monteur = m;
			}
		}
		
		Klus klus = new Klus(klant, type, monteur, auto, new ArrayList<Artikel>());
		int klusid = b.getPlanning().addKlus(klus, weeknr, jaar, dag, tijd);
		
		rd = req.getRequestDispatcher("klusinfo.jsp?klusid="+klusid);
		rd.forward(req,res);
	}
}
