package werkplaats.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import voorraadbeheer.Artikel;
import voorraadbeheer.Brandstof;
import voorraadbeheer.Onderdeel;
import werkplaats.Klus;
import bedrijf.Bedrijf;

public class EditKlusServlet extends HttpServlet{
	private static final long serialVersionUID = 8307356313786847101L;
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		
		Bedrijf bedrijf = (Bedrijf) req.getServletContext().getAttribute("Bedrijf");
		
		int klusid = Integer.parseInt(req.getParameter("klusid"));
		
		Artikel art = bedrijf.getCatalogus().getArtikel(req.getParameter("artikel"));
		if(art.getArtikelType().equals("Brandstof")) {
			double liters = Double.parseDouble(req.getParameter("aantal"));
			Brandstof bra = (Brandstof) art;
			bra.setLiter(bra.getLiter() - liters);
		} else {
			int aantal = Integer.parseInt(req.getParameter("aantal"));
			Onderdeel ond = (Onderdeel) art;
			ond.setAantal(ond.getAantal() - aantal);
		}
		
		Klus klus = bedrijf.getPlanning().getKlusById(klusid);
		klus.addGebruikteArtikel(art);
		
		bedrijf.getPlanning().editKlus(klus, klusid);
		
		rd = req.getRequestDispatcher("klusinfo.jsp?klusid="+klusid);
		rd.forward(req,res);
	}
}
