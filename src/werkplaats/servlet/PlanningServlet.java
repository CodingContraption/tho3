package werkplaats.servlet;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import werkplaats.Weekplanning;
import bedrijf.Bedrijf;

public class PlanningServlet extends HttpServlet{
	private static final long serialVersionUID = 2009733422266701844L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		RequestDispatcher rd = null;
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		
		Weekplanning planning = b.getPlanning();
		
		int week = 0;
        int jaar = Calendar.getInstance().get(Calendar.YEAR);
        if(req.getParameter("jaar") != null) {
            jaar = Integer.parseInt(req.getParameter("jaar"));
        }
		//edge case: jaarwisseling yay
		if(req.getParameter("weeknr") != null) {
			if(req.getParameter("iteration").equals("next")) {
				week = Integer.parseInt(req.getParameter("weeknr"));
				week++;
                if(week > 52) {
                    jaar++;
                    week = 1;
                }
			} else if(req.getParameter("iteration").equals("prev")) {
				week = Integer.parseInt(req.getParameter("weeknr"));
				week--;
                if(week < 1) {
                    jaar--;
                    week = 52;
                }
			}
		} else {
			week = planning.getWeekNr(Calendar.getInstance());
		}
		
        req.setAttribute("weekplanning", planning.getWeekPlanning(week, jaar));
        req.setAttribute("dagenvdweek", planning.getDagenWeek(week, jaar));
        req.setAttribute("weeknr", week);
        req.setAttribute("jaar", jaar);
		
		rd = req.getRequestDispatcher("planning.jsp");
		rd.forward(req,res);
	}
}
