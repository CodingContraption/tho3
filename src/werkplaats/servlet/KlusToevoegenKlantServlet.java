package werkplaats.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bedrijf.Bedrijf;
import klantenbinding.Klant;

public class KlusToevoegenKlantServlet extends HttpServlet{
	private static final long serialVersionUID = 6162181686589112437L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		Bedrijf b = (Bedrijf) req.getServletContext().getAttribute("Bedrijf");
		Klant k = b.getKlant(req.getParameter("klant"));
		req.setAttribute("klant", k);
		req.setAttribute("type", req.getParameter("type"));
		req.setAttribute("monteurid", req.getParameter("monteur"));
		
		req.setAttribute("weeknr", req.getParameter("weeknr"));
		req.setAttribute("jaar", req.getParameter("jaar"));
		req.setAttribute("dag", req.getParameter("dag"));
		req.setAttribute("tijd", req.getParameter("tijd"));
		
		RequestDispatcher rd = null;
		rd = req.getRequestDispatcher("klustoevoegenauto.jsp");
		rd.forward(req,res);
	}
}