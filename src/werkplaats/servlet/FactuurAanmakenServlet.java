package werkplaats.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import werkplaats.Factuur;
import werkplaats.Klus;
import bedrijf.Bedrijf;

public class FactuurAanmakenServlet extends HttpServlet{
	private static final long serialVersionUID = -1183372318276389185L;

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		Bedrijf b = (Bedrijf)req.getServletContext().getAttribute("Bedrijf");
		int klusid	 = Integer.parseInt(req.getParameter("klusid"));
		Klus klus = b.getPlanning().getKlusById(klusid);
		
		Factuur factuur = new Factuur(klus);
		
		req.setAttribute("factuur", factuur);
		
		RequestDispatcher rd = null;
		rd = req.getRequestDispatcher("factuur.jsp");
		rd.forward(req,res);
	}
}
