package werkplaats;

import java.io.Serializable;

public class Monteur implements Serializable {
	private static final long serialVersionUID = 6533930557904294196L;
	private String naam;
	private int monteurID;
	
	public Monteur(String naam, int ID){
		this.naam = naam;
		monteurID = ID;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public int getMonteurID() {
		return monteurID;
	}

	public void setMonteurID(int monteurID) {
		this.monteurID = monteurID;
	}
}
