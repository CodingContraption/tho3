package werkplaats;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import klantenbinding.Auto;
import klantenbinding.Klant;
import voorraadbeheer.Artikel;
import voorraadbeheer.Brandstof;
import voorraadbeheer.Onderdeel;

public class Factuur {
	private Klant klant;
	private Auto auto;
	private String factuurnummer;
	private ArrayList<Artikel> gebruikteArtikelen;

	private Calendar date;

	public Factuur(Klus klus) {
		super();
		this.klant = klus.getKlant();
		this.auto = klus.getBetrokkenAuto();
		this.date = Calendar.getInstance();
		this.gebruikteArtikelen = klus.getGebruikteArtikelen();

		int fNummer = 0;
		while (fNummer < 10000000) {
			fNummer = new Random().nextInt(100000000);
		}
		this.factuurnummer = fNummer + "";

	}

	public String getFactuurnummer() {
		return factuurnummer;
	}
	
	public String schrijfArtikellenUit(){
		String returnString = "";
		
		int eindbedrag = 0;
		
		for (Artikel a : gebruikteArtikelen){
			if (a.getArtikelType() == "brandstof"){
				Brandstof b = (Brandstof) a;
				double totalePrijs = b.getLiter() * b.getPrijsPerLiter();
				returnString = returnString + "\n" + b.getName() + " -> " + b.getPrijsPerLiter() + " * " + b.getLiter() + "  = &euro;" +  totalePrijs;
				eindbedrag += totalePrijs;
			}
			else{
				Onderdeel o = (Onderdeel) a;
				double totalePrijs = o.getPrijsPerStuk()*o.getAantal();
				returnString = returnString + "\n" + o.getName() + " -> " + o.getPrijsPerStuk() + " * " + o.getAantal() + "  = &euro;" +  totalePrijs;
				eindbedrag += totalePrijs;
			}
		}
		
		returnString += "<br>Totaalprijs: &euro;"+eindbedrag;
		
		return returnString;
	}

	public void setFactuurnummer(String factuurnummer) {
		this.factuurnummer = factuurnummer;
	}

	public Klant getKlant() {
		return klant;
	}

	public void setKlant(Klant klant) {
		this.klant = klant;
	}

	public Auto getAuto() {
		return auto;
	}

	public void setAuto(Auto auto) {
		this.auto = auto;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public ArrayList<Artikel> getGebruikteOnderdelen() {
		return gebruikteArtikelen;
	}

	public void setGebruikteOnderdelen(ArrayList<Artikel> gebruikteOnderdelen) {
		this.gebruikteArtikelen = gebruikteOnderdelen;
	}

}
