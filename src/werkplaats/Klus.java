package werkplaats;

import java.util.ArrayList;

import klantenbinding.Auto;
import klantenbinding.Klant;
import voorraadbeheer.Artikel;
import voorraadbeheer.Brandstof;
import voorraadbeheer.Onderdeel;

public class Klus {
	private String type;
	private Monteur betrokkenMonteur;
	private Auto betrokkenAuto;
	private ArrayList<Artikel> gebruikteArtikelen;
	private Klant klant;
		
	public Klus(Klant klant, String type, Monteur betrokkenMonteur, Auto betrokkenAuto, ArrayList<Artikel> gebruikteArtikelen) {
		this.type = type;
		this.betrokkenMonteur = betrokkenMonteur;
		this.betrokkenAuto = betrokkenAuto;
		this.gebruikteArtikelen = gebruikteArtikelen;
		this.klant = klant;
	}
	
	public Klant getKlant() {
		return klant;
	}

	public void setKlant(Klant klant) {
		this.klant = klant;
	}

	public Monteur getBetrokkenMonteur() {
		return betrokkenMonteur;
	}
	
	public void setBetrokkenMonteur(Monteur betrokkenMonteur) {
		this.betrokkenMonteur = betrokkenMonteur;
	}
	
	public Auto getBetrokkenAuto() {
		return betrokkenAuto;
	}
	
	public void setBetrokkenAuto(Auto betrokkenAuto) {
		this.betrokkenAuto = betrokkenAuto;
	}
	
	public ArrayList<Artikel> getGebruikteArtikelen() {
		return gebruikteArtikelen;
	}
	
	public void setGebruikteArtikelen(ArrayList<Artikel> gebruikteArtikelen) {
		this.gebruikteArtikelen = gebruikteArtikelen;
	}
	
	public void addGebruikteArtikel(Artikel artikel) {
		if(artikel.getArtikelType().equals("Brandstof")) {
			Brandstof brandstof = (Brandstof) artikel;
			if(!gebruikteArtikelen.contains(brandstof)) {
				gebruikteArtikelen.add(brandstof);
			}
		} else if(artikel.getArtikelType().equals("Onderdeel")) {
			Onderdeel onderdeel = (Onderdeel) artikel;
			if(!gebruikteArtikelen.contains(onderdeel)) {
				gebruikteArtikelen.add(onderdeel);
			}
		}
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
}
